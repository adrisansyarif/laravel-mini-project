<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Models\Country;
use Illuminate\Support\Facades\DB;
use App\Models\Post;
use App\Models\User;
use App\Models\Role;
use App\Models\Photo;
use App\Models\Tag;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/about', function () {
//     return "Hi about page";

// });

// Route::get('/contact', function () {
//     return "Hi contact page";
// });

// Route::get('/post/{id}/{name}' , function($id, $name){

//     return "This is post number " . $id . " " . $name;
// });

// Route::get('admin/posts/example',['as'=>'admin.home', function(){

//     $url = route('admin.home');

//     return "this url " . $url;
// }]);
// 
// Route::get('/contact', [PostController::class, 'contact']);

// Route::resource('posts', 'App\Http\Controllers\PostController');

// Route::get('post/{id}/{name}/{password }', [PostController::class, 'showPost']);

/*
|--------------------------------------------------------------------------
| DATABASE RAW SQL QURRY
|--------------------------------------------------------------------------
*/

// Route::get('/insert', function(){
//     DB::insert('insert into posts(title, body) values(?, ?)', ['PHP Laravel is awesome', 'PHP Laravel is the best thing that has happened in PHP']);
// });


// Route::get('/read', function(){
//     $result = DB::select('SELECT * FROM posts WHERE id = ?', [1]);
//     return $result;
//     // foreach($result as $post){
//     //     return $post->title;    
//     // }
// });

// Route::get('/update', function(){
//     $update = DB::update('UPDATE posts SET title = "Update Title" WHERE id = ?', [1] );
//     return $update;
// });

// Route::get('/delete', function() {
//     $deleted = DB::delete('DELETE FROM posts WHERE id = ?', [1]);
//     return $deleted;
// });


/*
|--------------------------------------------------------------------------
| ELOQUENT ROM
|--------------------------------------------------------------------------
*/

//Read-------------------------------------
// Route::get('/read', function() {

//     $posts = Post::all();
//     foreach($posts as $post) {
//         return $post->title;
//     }

// });

// Route::get('/find', function() {

//     $posts = Post::find(2);

//     return $posts;

// });

// Route::get('/findwhere', function() {
//     $posts = Post::where('id', 3)->orderBy('id', 'desc')->take(1)->get();

//     return $posts;
// });

// Route::get('/findmore', function() {
//     // $posts = Post::findOrFail(1);

//     // return $posts;

//     $posts = Post::where('users_count', '<', 50)->firstOfFail();
// });


//CREATE----------------------------
// Route::get('/basicinsert', function() {
//     $posts = new Post();

//     $posts->title = 'new ORM title';
//     $posts->body = "Woww ORM is cool";

//     $posts->save();
// });

// Route::get('/create', function() {
//     Post::create(['title'=>'the created method', 'body' => 'Im learning alot ']);
// });

//UPDATE--------------------------------
// Route::get('/basicinsert', function() {
//     $posts = Post::find(2);

//     $posts->title = 'new ORM title';
//     $posts->body = "Woww ORM is cool";

//     $posts->save();
// });

// Route::get('/update', function(){

//     Post::where('id', 3)->where('is_admin', 0)->update(['title'=>'new title', 'body'=>'new body']);
// });

//DELETE--------------------------------
// Route::get('/delete', function() {
//     $posts = Post::find(2);
//     $posts->delete();
// });

// Route::get('/delete2', function(){
//     Post::destroy(3);
// });


// Route::get('/delete2', function(){
//     Post::destroy([4,5]);

//     Post::where('is_admin', 0)->delete();
// });


// Route::get('/delete3', function(){
    
//     Post::where('is_admin', 0)->delete();
// });

//SOFT DELETE---------------------------

// Route::get('/softdelete', function(){

//     Post::find(8)->delete();
// });


//RETRIEVING DELETE------------------------
// Route::get('/readsoftdelete', function(){

//     //    $posts = Post::find(5);
//     //    return $posts;
    
//     // $post = Post::withTrashed()->where('id', 5)->get();
//     // return $post;

//     $post = Post::onlyTrashed()->where('is_admin', 0)->get();
//     return $post;
// });

//RESTORING DELETE-----------------------------
// Route::get('/restore', function() {
//     Post::withTrashed()->where('is_admin', 0)->restore();
// });

//DELETE PERMANENT

// Route::get('/forcedelete', function() {
//     Post::onlyTrashed()->where('is_admin', 0)->forceDelete();
// });

/*
|--------------------------------------------------------------------------
| ELOQUENT Relationships
|--------------------------------------------------------------------------
*/

// //One to One Relationship
// Route::get('/user/{id}/post', function($id){
//     return User::find($id)->post->body;
// });

// //Inverse relation
// Route::get('/post/{id}/user', function($id){
//     return Post::find($id)->user->email;
// });

//  //One to One Relationship
// Route::get('/post', function(){
//     $user = User::find(1);

//     foreach($user->posts as $post) {
//         echo $post->title . "<br>";
//     }
// });

// //Many to Many Relationship
// Route::get('/user/{id}/role', function($id){
//     $user =  User::find($id)->roles()->orderBy('id', 'desc')->get();
    
//     return $user;
//     // foreach($user->roles as $role) {
//     //     return $role -> name . "<br>";
//     // }
//  });

// Route::get('/role/{id}/user', function($id) {

//     return Role::find($id)->name;

// });

// //Accessing the intermediate table/ pivot

// Route::get('user/pivot', function(){
//     $user = User::find(1);
//     foreach($user->roles as $role) {
//         echo $role->pivot->created_at;
//     }
// });

// Route::get('/user/{id}/country', function ($id){
//     $country = Country::find($id);
    
//     foreach($country->posts as $post) {

//         return $post->title;
//     }
// });

// //Polymorphic

// Route::get('/user/{id}/photos', function($id) {
//     $user = User::find($id);
    
//     foreach($user->photos as $photo) {
//         // var_dump($photo);
//         return $photo;
//     }
// });

// Route::get('/post/{id}/photos', function($id) {
//     $post = Post::find($id);
    
//     foreach($post->photos as $photo) {
//         // var_dump($photo);
//         echo $photo . "<br>";
//     }
// });

// Route::get('/photo/{id}/post', function($id){
//     $photo = Photo::findOrFail($id);

//     return $photo->imageable;
// });

// Polymorphic Many to Many

// Route::get('post/{id}/tag', function($id) {

//     $post = Post::find($id);

//     foreach($post->tags as $tag) {

//         echo $tag;

//     }

// });

// Route::get('video/{id}/tag', function($id) {

//     $video = Video::find($id);

//     foreach($video->tags as $tag) {

//         echo $tag;

//     }

// });

// Route::get('/tag/{id}/post', function($id) {
//     $tag = Tag::find($id);

//     foreach($tag->posts as $post) {

//         echo $post->body;

//     }
// });

// Route::get('/tag/{id}/video', function($id) {
//     $tag = Tag::find($id);

//     foreach($tag->videos as $video) {

//         echo $video->name;

//     }
// });

/*
|--------------------------------------------------------------------------
| CRUD APLICATION
|--------------------------------------------------------------------------
*/

Route::resource('/posts', 'App\Http\Controllers\PostController');